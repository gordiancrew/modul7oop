public abstract class Game {
    private int rate;
    private String result;

    public void playGame() {
        printWelcomeText();
        rating();
        thisGameLogic();
        result();
    }

    public abstract void printWelcomeText();

    public void rating() {
        System.out.println("***\nВВЕДИТЕ СУММУ СТАВКИ($):");
        rate = Menu.scanner.nextInt();
        Account.getInstance().minusMoney(rate);
    }

    public void result() {
        Account.getInstance().plusMoney(rate);
        System.out.println("_________________________________________\n" + result +
                "\n-----------------------------------------\n" +
                "выйгрыш составил = " + rate + "$" +
                "\nсумма счета = " + Account.getInstance().getMoney() +
                "\n-----------------------------------------");

        System.out.println("***\nВВЕДИТЕ ЦИФРУ:\n" +
                "1-крутим игру\n" +
                "0-выход в меню");
        int a = Menu.scanner.nextInt();
        if (a == 0) {
            System.out.println("выходим");
        } else if (a == 1) {
            playGame();
        } else {
            System.out.println("Некорректный ввод");
        }
    }

    public abstract void thisGameLogic();

    public void setResult(String result) {
        this.result = result;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public int getRate() {
        return rate;
    }

    public String getResult() {
        return result;
    }
}
