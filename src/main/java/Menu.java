import java.util.Scanner;

public class Menu {
    private static boolean gameGo = true;
    public static Scanner scanner;

    public static void main(String[] args) {
        scanner = new Scanner(System.in);
        System.out.println("ПРИВЕТСТВУЕМ ВАС В НАШЕМ КАЗИНО!\nПожалуйста подтвердите что вам есть 18 лет." +
                "\n***\n" +
                "ВВЕДИТЕ ЦИФРУ:\n" +
                "1-если вам уже исполнилось 18лет.\n" +
                "2-если вам еще не исполнилось 18 лет.");
        int input18 = scanner.nextInt();
        if (input18 == 1) {
            System.out.println("Отлично! Вы можете играть" +
                    "\nВведите ваше имя и сумму денег на игру." +
                    "\n***\n" +
                    "ВВЕДИТЕ ВАШЕ ИМЯ:");
            String name = scanner.next();
            System.out.println("Приятно познакомится " + name + "!" +
                    "\n***\n" +
                    "ВВЕДИТЕ СУММУ ДЕНЕГ($):");
            int money = scanner.nextInt();
            System.out.println("Замечательно! Сумма " + money + "$ у вас на счету!\nДобро пожаловать в меню:");
            Account account = Account.getInstance();
            account.setMoney(money);
            account.setName(name);
            while (gameGo) {
                menu();
            }
        } else
            System.out.println("К сожалению вам еще нельзя принимать участие в нашем казино.\n" +
                    "Увидемся , когда вам исполнится 18)");
    }

    public static void menu() {

        GamesFactory gamesFactory = new GamesFactory();
        TypeGame typeGame = null;
        System.out.println("Выберите во что хотите поиграть:\n" +
                "***\n" +
                "ВВЕДИТЕ ЦИФРУ:\n" +
                "1-игровой автомат три лимона\n" +
                "2-рулетка\n" +
                "0-выход из казино");
        int select = scanner.nextInt();

        switch ( select){

            case 0:
                Account.getInstance().exitGame();
                break;
            case 1:
                typeGame=TypeGame.THREE_LEMON;
                break;
            case 2:
                typeGame=TypeGame.RULET;
                break;
            default:
                System.out.println("Некорректный ввод");
                break;
        }
        if(typeGame!=null){

            gamesFactory.createGame(typeGame).playGame();
        }
    }

    public static void setGameGo(boolean gameGo) {
        Menu.gameGo = gameGo;
    }
}




