import static java.lang.Math.random;

public class ThreeLemons2 extends Game {
    private static ThreeLemons2 instance;

    public static ThreeLemons2 getInstance() {
        if (instance == null) {
            instance = new ThreeLemons2();
        }
        return instance;
    }

    @Override
    public void printWelcomeText() {
        System.out.println("Играем в три лимона");
    }

    @Override
    public void thisGameLogic() {
        int rate=getRate();
        String[] fruits = {"lemon", "cherry", "banan", "apple"};
        String a = fruits[(int) (random() * 4)];
        String b = fruits[(int) (random() * 4)];
        String c = fruits[(int) (random() * 4)];
        if (a == b && b == c) {
            rate *= 14;
        } else if (a == b || a == c || b == c) {
            rate *= 2;
        } else {
            rate = 0;
        }
        setRate(rate);
        setResult( a + "-" + b + "-" + c);
    }
}
