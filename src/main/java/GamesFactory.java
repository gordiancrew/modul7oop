public class GamesFactory {
    public Game createGame(TypeGame typeGame) {
        Game game = null;
        switch (typeGame) {

            case THREE_LEMON:
                game = ThreeLemons2.getInstance();
                break;
            case RULET:
                game = Rulet.getInstance();
                break;
        }
        return game;
    }
}
