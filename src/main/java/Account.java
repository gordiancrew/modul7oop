public class Account {
    private String name;
    private int money;
    private static Account instance;

    public static Account getInstance() {
        if (instance == null) {
            instance = new Account();
        }
        return instance;
    }

    public void exitGame() {
        System.out.println("До новых встреч!\n" +
                "Сумма вашего счета " + Account.getInstance().getName() + " составила " +
                getMoney());
        Menu.setGameGo(false);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public void minusMoney(int minus) {
        money -= minus;
    }

    public void plusMoney(int plus) {
        money += plus;
    }
}
